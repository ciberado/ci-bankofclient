/*global describe, it */
'use strict';

(function () {
    describe('Funcionalidad de transferencias.', function () {
        it('Una transferencia no puede tener el mismo origen y destino.', function () {
            var transferencia = new OrdenTransferencia('1111', '1111', 1);
            var ejecucionPeligrosa = function () {
                transferencia.validar();
            };
            expect(ejecucionPeligrosa).toThrow();
            expect(transferencia.status).toBe(OrdenTransferencia.status.VALIDADA_ERROR);
        });

        it('Una transferencia valida correctamente.', function () {
            var transferencia = new OrdenTransferencia('1111', '2222', 1);
            var ejecucionPeligrosa = function () {
                transferencia.validar();
            };
            expect(ejecucionPeligrosa).not.toThrow();
            expect(transferencia.status === OrdenTransferencia.status.VALIDADA_OK);
        });

        it('No es posible procesar una trasnferencia no validada.', function () {
            var transferencia = new OrdenTransferencia('1111', '2222', 1);
            var ejecucionPeligrosa = function () {
                transferencia.procesar();
            };
            expect(ejecucionPeligrosa).toThrow();
        });

        it('Procesar una transferencia debería contactar con /transferencias', function (doneAsync) {
            $.mockjax({
                url: /\/transferencias$/,
                status: 201,
                responseText: {
                    "id": 2,
                    "origen": {
                        "codigo": "1111",
                        "saldo": 999,
                        "interesAnual": 0.02
                    },
                    "fecha": 1433943528040,
                    "importe": 1
                }
            });

            var transferencia = new OrdenTransferencia('1111', '2222', 1);
            transferencia.validar();
            var promesa = transferencia.procesar();
            promesa.done(function (respuesta) {
                expect(true).toBe(true);
                doneAsync();
            });
            promesa.fail(function (response) {
                throw new Error('Respuesta procesada incorrectamente.')
            })
        })
    });
})();
